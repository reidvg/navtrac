FROM node:15.7-alpine As development

WORKDIR /usr/src/app

COPY yarn.lock .
COPY package.json .

RUN yarn install --only=development

COPY . .

RUN yarn build

FROM node:15.7-alpine as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}