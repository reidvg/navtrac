# Starting the APP in dev mode

# Running the db

run 

    docker-compose up

this will expose the db on port 5432

This is already set as the demo in the database providers, 
there needs to be a config in production. 

create a database

    # su - postgres
    postgres@9111d920b0e2:~$ psql
    psql (13.3 (Debian 13.3-1.pgdg100+1))
    Type "help" for help.
    
    postgres=# CREATE DATABASE secrets;
    CREATE DATABASE
    postgres=# \list
    List of databases
    Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
    -----------+----------+----------+------------+------------+-----------------------
    postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
    secrets   | postgres | UTF8     | en_US.utf8 | en_US.utf8 |
    template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
    |          |          |            |            | postgres=CTc/postgres
    template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
    |          |          |            |            | postgres=CTc/postgres
    (4 rows)


# Tests

    jest 

# Import data

    npx nestjs-command ./cli.ts 

