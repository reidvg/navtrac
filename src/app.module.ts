import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { databaseProviders } from './database.providers';
import { RecordsModule } from './records/records.module';
import { CsvparserModule } from './csvparser/csvparser.module';
import { CommandModule } from "nestjs-command";

@Module({
  imports: [RecordsModule, CsvparserModule, CommandModule],
  controllers: [AppController],
  providers: [AppService, ...databaseProviders],
  exports: [...databaseProviders],
})
export class AppModule {}
