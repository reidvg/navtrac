import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { RecordsModule } from './records/records.module';
import { RecordsService } from './records/records.service';
import { CsvparserModule } from './csvparser/csvparser.module';
import { CsvparserService } from './csvparser/csvparser.service';
import { Record } from './records/record.entity';
import { Md5 } from 'ts-md5';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const csv = require('csv-parser');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require('fs');
const results = [];

(async () => {
  const app = await NestFactory.createApplicationContext(AppModule, {
    logger: false,
  });

  await app
    .select(CsvparserModule)
    .get(CsvparserService)
    .getData()
    .toPromise()
    .then((data) => {
      const recordService = app.select(RecordsModule).get(RecordsService);
      fs.writeFileSync('./data.csv', data);
      fs.createReadStream('./data.csv')
        .pipe(csv())
        .on('data', async function (record: Record) {
          console.log('Creating Record ' + record.key);
          record.value = Md5.hashStr(record.value);
          await recordService.create(record).catch((error) => {
            // @TODO Catch dupe error
            console.log(error.message);
          });
        });
    });
})();
