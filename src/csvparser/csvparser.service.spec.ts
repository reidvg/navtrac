import { Test, TestingModule } from '@nestjs/testing';
import { CsvparserService } from './csvparser.service';
import { HttpModule } from '@nestjs/common';

describe('CsvparserService', () => {
  let service: CsvparserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CsvparserService],
      imports: [HttpModule],
    }).compile();

    service = module.get<CsvparserService>(CsvparserService);
  });

  it('should be able to get data', async () => {
    service
      .getData()
      .toPromise()
      .then((data) => {
        expect(data).not.toBe(null);
      });
  });
});
