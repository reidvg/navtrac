import { HttpService, Injectable } from '@nestjs/common';
import { map } from 'rxjs/operators';

@Injectable()
export class CsvparserService {
  constructor(protected httpService: HttpService) {}

  getData() {
    return this.httpService
      .get('https://share.getcloudapp.com/eDuwq7xR/download/input.csv')
      .pipe(
        map((response) => {
          return response.data;
        }),
      );
  }
}
