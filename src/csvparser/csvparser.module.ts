import { HttpModule, Module } from '@nestjs/common';
import { CsvparserService } from './csvparser.service';

@Module({
  providers: [CsvparserService],
  imports: [HttpModule],
})
export class CsvparserModule {}
