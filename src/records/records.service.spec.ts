import { Test, TestingModule } from '@nestjs/testing';
import { RecordsService } from './records.service';
import { recordProviders } from './record.provider';
import { RecordsController } from './records.controller';
import { databaseProviders } from '../database.providers';
import { Record } from './record.entity';
import { Md5 } from 'ts-md5';
import * as faker from 'faker';

describe('RecordsService', () => {
  let service: RecordsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [...recordProviders, RecordsService, ...databaseProviders],
      controllers: [RecordsController],
    }).compile();

    service = module.get<RecordsService>(RecordsService);
  });

  afterEach(async (done) => {
    // Closes active connections
    await done();
  });

  it('should create record', async () => {
    const entity = new Record();
    entity.comment = 'Comments';
    entity.key = faker.datatype.uuid();
    entity.encrypted_value = Md5.hashStr('value');
    const newEntity = await service.create(entity);
    expect(newEntity.id).not.toBe(undefined);
  });
});
