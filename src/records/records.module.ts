import { forwardRef, Module } from '@nestjs/common';
import { RecordsService } from './records.service';
import { AppModule } from '../app.module';
import { recordProviders } from './record.provider';
import { RecordsController } from './records.controller';

@Module({
  imports: [forwardRef(() => AppModule)],
  providers: [...recordProviders, RecordsService],
  controllers: [RecordsController],
})
export class RecordsModule {}
