import { Test, TestingModule } from '@nestjs/testing';
import { RecordsController } from './records.controller';
import { recordProviders } from './record.provider';
import { RecordsService } from './records.service';
import { databaseProviders } from '../database.providers';

describe('RecordsController', () => {
  let controller: RecordsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [...recordProviders, RecordsService, ...databaseProviders],
      controllers: [RecordsController],
    }).compile();

    controller = module.get<RecordsController>(RecordsController);
  });

  afterEach(async (done) => {
    // Closes active connections
    await done();
  });

  it('should findAll and not be undefined', () => {
    expect(controller.findAll()).not.toBe(undefined);
  });
});
