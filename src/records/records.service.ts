import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Record } from './record.entity';

@Injectable()
export class RecordsService {
  constructor(
    @Inject('RECORD_REPOSITORY')
    private recordRepository: Repository<Record>,
  ) {}

  async findAll(): Promise<Record[]> {
    return this.recordRepository.find();
  }

  async create(entity: Record): Promise<Record> {
    return this.recordRepository.save(entity);
  }
}
